## Axure RP 10 简体中文语言包

本语言包在Windows10系统中通过测试，可放心使用，仅适用于Axure RP 10！

### 【文件版本】
语言包文件版本：Axure RP 10 CHS v1.4.1 for 3829

### 【使用方法】
1、将语言包压缩包解压缩。

2、将解压缩后的lang文件夹（软件语言包文件）和DefaultSettings文件夹（网页语言包文件）复制粘贴到软件安装根目录下。

### 【Windows系统语言包路径】
以Win10系统为例：C:\Program Files\Axure\Axure RP 10

### 【Mac系统语言包路径】
以中文版为例：前往--应用程序--右键点击程序图标--显示包内容，依次打开文件夹：Contents>MacOS>

### 【注意】
1、Axure RP 10语言包文件为通用语言包，理论支持Axure RP 6/7/8/9版本，但有可能语言包不全。

2、Axure RP 10网页语言包为专用语言包，只能对应客户端版本使用，版本不同可能出现不可预料的结果，切记。

3、语言包前请备份需要替换的文件，如果替换文件导致软件无法打开，请恢复备份的文件。

4、不定时跟进新版Axure RP 10的语言包，欢迎关注本仓库下载最新版语言包。

### 【申明】
软件通用语言包语言文件基于Pluwen发布的Axure RP 10中文语言包整理补全，感谢小楼老师及曾经为该语言包贡献的朋友们！欢迎大家过来贡献翻译，保持该软件语言包的持续更新！

### 【截图演示】
[![软件界面截图](https://gitee.com/shileiye/Axure-RP-10-CHS/raw/main/%E6%BC%94%E7%A4%BA%E5%9B%BE%E7%89%87/%E8%BD%AF%E4%BB%B6%E6%88%AA%E5%9B%BE.png "软件界面截图")](https://gitee.com/shileiye/Axure-RP-10-CHS/raw/main/%E6%BC%94%E7%A4%BA%E5%9B%BE%E7%89%87/%E8%BD%AF%E4%BB%B6%E6%88%AA%E5%9B%BE.png "软件界面截图")

[![网页界面截图](https://gitee.com/shileiye/Axure-RP-10-CHS/raw/main/%E6%BC%94%E7%A4%BA%E5%9B%BE%E7%89%87/%E7%BD%91%E9%A1%B5%E6%BC%94%E7%A4%BA%E6%88%AA%E5%9B%BE.png "网页界面截图")](https://gitee.com/shileiye/Axure-RP-10-CHS/raw/main/%E6%BC%94%E7%A4%BA%E5%9B%BE%E7%89%87/%E7%BD%91%E9%A1%B5%E6%BC%94%E7%A4%BA%E6%88%AA%E5%9B%BE.png "网页界面截图")

### 【Axure RP 10 官方简介（机翻）】
[![Axure RP 10官方简介（机翻）](https://gitee.com/shileiye/Axure-RP-10-CHS/raw/main/%E6%BC%94%E7%A4%BA%E5%9B%BE%E7%89%87/Axure%20RP%2010%E4%BB%8B%E7%BB%8D.png "Axure RP 10 官方简介（机翻）")](https://gitee.com/shileiye/Axure-RP-10-CHS/raw/main/%E6%BC%94%E7%A4%BA%E5%9B%BE%E7%89%87/Axure%20RP%2010%E4%BB%8B%E7%BB%8D.png "Axure RP 10 官方简介（机翻）")